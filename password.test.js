const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })

  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has least 1 number (0-9) in password', () => {
    expect(checkDigit('m0m')).toBe(true)
  })

  test('should not has least 1 number (0-9) in password', () => {
    expect(checkDigit('lnW')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol ! in password to be true', () => {
    expect(checkSymbol('11!11')).toBe(true)
  })

  test('should not has symbol ! in password to be false', () => {
    expect(checkSymbol('11l11')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password Boyza@00777 to be true', () => {
    expect(checkPassword('Boyza@00777')).toBe(true)
  })

  test('should password 77777777 to be false', () => {
    expect(checkPassword('77777777')).toBe(false)
  })
})
